#include <stdio.h>
#include <string.h>
#include <cstdint>


inline uint64_t atoui(const char * b, const char * e) {
  uint64_t result = 0;
  for(; b != e; ++b) {
    //printf("xxx\n");
    result = result * 10 + (*b - '0');
    //printf("xxx\n");
  }
  return result;
}

int main(void) {

  char * dst = (char *)"12345678910";
  
  uint64_t result = atoui(dst, dst + strlen(dst));
  printf("result = %ld\n", result);
  return 0;

}
