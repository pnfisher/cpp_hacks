#include <iostream>

template<typename D, typename B>
struct isDerivedFrom {
 private:
  struct No {};
  struct Yes { int64_t x[10]; };

  static Yes Test(B*);
  static No Test(...);

 public:

  const static bool value = sizeof(Test(static_cast<D*>(0))) == sizeof(Yes);

};

class B {

};

class D : public B {

};

class A {

};

int main(void) {

  std::cout
    << "B is derived from D: "
    << std::boolalpha
    << isDerivedFrom<B,D>::value
    << std::endl;

  std::cout
    << "D is derived from B: "
    << std::boolalpha
    << isDerivedFrom<D,B>::value
    << std::endl;

  return 0;
  
}

