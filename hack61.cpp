#include <iostream>

struct B {

  void print(void) { msg(); }
  virtual void msg(void) { std::cout << "This is B\n"; }

};

struct D : public B {

  void msg(void) { std::cout << "This is D\n"; }

};

int main(void) {

  D d;

  d.print();

  return 0;

}
