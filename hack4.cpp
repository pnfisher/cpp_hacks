#include <stdio.h>
#include <assert.h>
#include <string.h>

#include <iterator>
#include <cstdint>
#include <ctime>

uint32_t u64toAsciiClassic(uint64_t value, char * dst) {

  auto start = dst;

  do {
    *dst++ = '0' + (value % 10);
    value /= 10;
  } while (value != 0);

  const uint32_t result = dst - start;

  for(dst--; dst > start; start++, dst--) {
    std::iter_swap(dst, start);
  }

  return result;
  
}

inline void invalidate_cache(void) {

  static const uint32_t nbytes = (8192*1024) >> 2;
  static volatile uint32_t buf [nbytes], sum;

  sum = 0;
  for(uint32_t i = 0; i < nbytes; i++) {
    //sum += buf[i];
    buf[i] = 0;
  }

}

inline uint64_t atoui(const char * b, const char * e) {
  uint64_t result = 0;
  //printf("xxx\n");
  for(; b != e; ++b) {
    //printf("xxx\n");
    result = result * 10 + (*b - '0');
    //printf("xxx\n");
  }
  return result;
}

inline uint64_t atoui2(const char * b, const char * e) {
  static const uint64_t pow10[] = {
    10000000000000000000UL,
    1000000000000000000UL,
    100000000000000000UL,
    10000000000000000UL,
    1000000000000000UL,
    100000000000000UL,
    10000000000000UL,
    1000000000000UL,
    100000000000UL,
    10000000000UL,
    1000000000UL,
    100000000UL,
    10000000UL,
    1000000UL,
    100000UL,
    10000UL,
    1000UL,
    100UL,
    10UL,
    1UL,
  };
  uint64_t result = 0;
  auto i = sizeof(pow10) / sizeof(*pow10) - (e - b);
  //printf("xxx\n");
  for(; b != e; ++b) {
    //printf("xxx\n");
    result += pow10[i++] * (*b - '0');
    //printf("xxx\n");
  }
  return result;
}

int main(void) {

  const uint32_t loops = 0x8000000;
  //uint64_t value = (uint64_t)~0UL;
  char dst [32] = { 0 };
  int rc = 0;
  
  (void)u64toAsciiClassic((uint64_t)~0UL, dst);
  uint32_t len = strlen(dst);
  volatile uint64_t result = atoui(dst, dst + len); 
  printf("result = 0x%lx\n", result);
  result = atoui2(dst, dst + len);
  printf("result = 0x%lx\n", result);
  
  struct timespec start, stop;
  uint64_t nsecs = 0;
  for (uint64_t i = 0; i < loops; i++) {

    //invalidate_cache();
    memset(dst, 0, 32);
    
    (void)u64toAsciiClassic(0xffffffffffffff, dst);
    len = strlen(dst);

    rc = clock_gettime(CLOCK_MONOTONIC, &start);
    assert(!rc);
    result = atoui(dst, dst + len);
    rc = clock_gettime(CLOCK_MONOTONIC, &stop);
    assert(!rc);

    assert(result == 0xffffffffffffff);
    
    nsecs += (stop.tv_sec * 1e9) + stop.tv_nsec;
    nsecs -= (start.tv_sec * 1e9) + start.tv_nsec;

  }


  printf("time = %.5f\n", nsecs/1e9);


}
