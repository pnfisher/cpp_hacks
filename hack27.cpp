#include <array>
#include <algorithm>
#include <deque>
#include <iostream>
#include <list>
#include <vector>

template <template <class, class...> class C, class V, class... A>
void
print_elements(const C<V, A...>& c)
{
  for (auto& it : c)
    std::cout << it << " ";
  std::cout << std::endl;
}

template <typename I>
std::pair<I, I>
slide(I f, I l, I p)
{
  std::cout << std::distance(p, f) << std::endl;
  if (std::distance(p, f) > 0)
    return {p, rotate(p, f, l)};
  if (std::distance(p, f) < 0)
    return {rotate(f, l, p), p};
  return {f, l};
}

int
main(void)
{

  std::list<int> ints{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

  {
    auto ints2(ints);
    print_elements(ints2);
    auto b = ints2.begin(); std::advance(b, 3);
    auto e = ints2.end(); std::advance(e, -1);
    auto p = ints2.begin(); std::advance(p, 4);
    std::rotate(b, p, e);
    print_elements(ints2);
    std::iter_swap(b, --e);
    print_elements(ints2);

    //nth_element(ints.begin()
    
  }

  return 0;
}
