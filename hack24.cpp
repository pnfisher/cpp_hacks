#include <iostream>
#include <vector>
#include <array>

class Base
{

 public:
  Base() { std::cout << "Base ctor\n"; }
  Base(const std::string& msg)
  {
    std::cout << "Base copy ctor: " << msg << std::endl;
  }

  virtual void msg(void) const { std::cout << "Base msg\n"; }
  virtual ~Base() { std::cout << "Base dtor\n"; }
};

class Derived : public Base
{

 public:
  Derived()
      try : Base("hi")
  {
    std::cout << "Derived ctor\n";
  }
  catch (const std::exception& e) {
    std::cerr << "exception: " << e.what() << std::endl;
  }
  virtual void msg(void) const { std::cout << "Derived msg\n"; }
  virtual ~Derived() { std::cout << "Derived dtor\n"; }
};

void
msg(const Base& d)
{

  d.msg();
}

int
main(void)
{

  Base base;
  Derived derived;

  msg(base);
  msg(derived);

  try {
    std::vector<int> v(5);
    v[1] = 1;
    std::cout << "v[10] = " << v[10] << std::endl;
    std::cout << "v.size() = " << v.size() << std::endl;
    v.at(4) = 4;
    std::cout << "v[4] = " << v[4] << std::endl;
    v.at(10) = 10;
  }
  catch(const std::exception& e) {
    std::cerr << "exception: " << e.what() << std::endl;
    std::abort();
  }

  return 0;
}
