#include <chrono>
#include <iostream>

int main(void) {

  std::cout << std::time(nullptr) << std::endl;
  std::srand(std::time(nullptr));
  std::cout << std::rand() << std::endl;
  std::cout << "RAND_MAX = " << RAND_MAX << std::endl;
  return 0;

}
