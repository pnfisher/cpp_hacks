#include <iostream>

class D
{
 public:
  virtual void foo() { std::cout << "Foooooo" << std::endl; }
};

class C : virtual public D
{
 public:
  void foo() { std::cout << "C Foo\n"; }
};

class B : virtual public D
{
 public:
  void foo() { std::cout << "B Foo\n"; }
};

class A : public B, public C
{
 public:
  void foo() { std::cout << "A Foo\n"; }
};

int
main(int argc, const char* argv[])
{
  A a;
  a.foo();
}
