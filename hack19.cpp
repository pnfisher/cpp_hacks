#include <iostream>

using namespace std;

struct A {

  int i;

  A() { i = 0; cout << "xxx A()\n"; };
  A(const A& a) { i = a.i; cout << "xxx A(const A& a)\n"; }

};

A foo() {

  A a;
  return a;

}

int main(void) {

  A a;
  A b(foo());

  return 0;

}
