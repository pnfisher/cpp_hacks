#include <stdio.h>
#include <stdint.h>
#include <limits.h>

int main(int argc, char *argv[])
{

  int c = INT_MAX;
  printf("c = %d, c = 0x%x\n", c, c);
  c = c + 1;
  printf("c = %d, c = 0x%x\n", c, c);
  
  printf("sizeof(int) = %lu\n", sizeof(int));
  printf("sizeof(long) = %lu\n", sizeof(long));
  printf("sizeof(long long) = %lu\n", sizeof(long long));

  printf("sizeof(double) = %lu\n", sizeof(double));
  printf("sizeof(long double) = %lu\n", sizeof(long double));
  
  return 0;

}
