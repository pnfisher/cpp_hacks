#include <algorithm>
#include <array>
#include <vector>
#include <cstdint>
#include <iostream>
#include <cassert>
#include <ctime>

using namespace std;

template <typename I>
inline void
print_elements(I b, I e)
{
  assert(e > b);
  std::cout << *b;
  for(b++; b != e; b++)
    std::cout << " " << *b;
  std::cout << std::endl;
}

template <typename C>
inline void
print_elements(const C& c)
{
  print_elements(c.begin(), c.end());
}

int
main(void)
{

  const uint32_t max_size = 100;

  array<int, max_size> ints;

  std::srand(std::time(nullptr));
  double scale = 10.0 / RAND_MAX;
  generate(ints.begin(), ints.end(), [=]() { return std::rand() * scale; });
  print_elements(ints);

  auto it = partition(ints.begin(), ints.end(), [](auto x) { return x & 1; });
  print_elements(ints);

  print_elements(ints.begin(), it);
  //copy(ints.begin(), it, ostream_iterator<int>(cout, " "));
  //cout << endl;
  print_elements(it, ints.end());
  //copy(it, ints.end(), ostream_iterator<int>(cout, " "));
  //cout << endl;
  
  return 0;
}
