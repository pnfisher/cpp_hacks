#include <cassert>
#include <algorithm>
#include <iostream>
#include <vector>
#include <list>
#include <iterator>

template <typename I>
std::pair<I, I>
slide(I f, I l, I p)
{

  std::cout << std::distance(p, f) << std::endl;
  
  if (std::distance(p, f) > 0) {
    std::cout << "xxx\n";
    return {p, rotate(p, f, l)};
  }
  if (std::distance(p, f) < 0)
    return {rotate(f, l, p), p};
  return {f, l};
}

typedef std::list<uint32_t> vec_t;

// template <typename T>
// static inline std::ostream&
// operator<<(std::ostream& out, std::list<T> const& v)
// {
//   out << '[';
//   if (!v.empty()) {
//     for (typename std::list<T>::const_iterator i = v.begin();;) {
//       out << *i;
//       if (++i == v.end())
//         break;
//       out << ", ";
//     }
//   }
//   out << ']';
//   return out;
// }

// inline void
// print_elements(vec_t::const_iterator b, vec_t::const_iterator e)
// {
//   assert(e > b);
//   e--;
//   for (auto it = b; it < e; it++)
//     std::cout << *it << " ";
//   std::cout << *e << std::endl;
//   // copy(f, l, std::ostream_iterator<T>(std::cout," "));
//   // for_each(f, l, [](auto& i) { std::cout << i << " "; });
//   // std::cout << std::endl;
// }

template <template <class, class...> class C, class V, class... Args>
inline void
print_elements(
  typename C<V, Args...>::const_iterator b,
  typename C<V, Args...>::const_iterator e)
{
  assert(e > b);
  e--;
  for (auto it = b; it < e; it++)
    std::cout << *it << " ";
  std::cout << *e << std::endl;
}

// template <template <class> class C, class V>
// inline void
// print_elements(typename C<V>::const_iterator b, typename C<V>::const_iterator
// e)
// {
//   assert(e > b);
//   e--;
//   for (auto it = b; it < e; it++)
//     std::cout << *it << " ";
//   std::cout << *e << std::endl;
// }

// template <template <class, class> class C, class V, class A>
// inline void
// print_elements(
//   typename C<V, A>::const_iterator b, typename C<V, A>::const_iterator e)
// {
//   assert(e > b);
//   e--;
//   for (auto it = b; it < e; it++)
//     std::cout << *it << " ";
//   std::cout << *e << std::endl;
// }

// inline void
// print_elements(const vec_t& c)
// {
//   print_elements(c.begin(), c.end());
// }

template<typename It>
inline void
print_elements(It b, It e) {
  assert(std::distance(b, e) > 0);
  std::cout << *b; b++;
  for(; b != e; b++)
    std::cout << " " << *b;
  std::cout << std::endl;
}

template <template <class, class...> class C, class V, class... A>
inline void
print_elements(const C<V, A...>& c)
{
  //print_elements<C, V, A...>(c.begin(), c.end());
  print_elements(c.begin(), c.end());
}

int
main(void)
{

  vec_t ints{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
  print_elements(ints);
  
  auto b = ints.begin();
  auto e = ints.end();
  auto p = ints.begin();

  std::advance(e, -2);
  std::advance(p, 3);


  auto pr = slide(b, p, e);
  print_elements(pr.first, pr.second);
  //std::rotate(b, p, e);
  print_elements(ints);

  const auto max = std::max_element(
    ints.begin(), ints.end(), [](const auto x, const auto y) { return y > x; });
  std::cout << *max << std::endl;

  auto filter = [](const auto x) {
    if (x > 5)
      std::cout << x << " ";
  };
  for_each(ints.begin(), ints.end(), filter);
  std::cout << std::endl;
  std::cout << "xxx\n";

#if 0
  nth_element(ints.begin(), ints.begin() + 3, ints.end());
  std::cout << ints[3] << std::endl;
#endif

  vec_t sorted_ints;
  copy(ints.begin(), ints.end(), std::back_inserter(sorted_ints));
  //sort(sorted_ints.begin(), sorted_ints.end());
  sorted_ints.sort();
  print_elements(sorted_ints);

  return 0;
}
