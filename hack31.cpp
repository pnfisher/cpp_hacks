#include <iostream>
#include <vector>
#include <memory>

using namespace std;

static int counter = 0;
static int id = 0;

class A {

  int n;
  int i;

  void report(string prefix) {
    cout << prefix << ": n = " << n << ", id = " << i << endl;
  }

 public:

  const A& operator=(const A& a) {
    report("operator=");
    return *this;
  }
  
  A() {
    n = 0;
    i = id++;
    counter++;
    report("ctor");
  }

  A(int x) {
    n = x;
    i = id++;
    counter++;
    report("custom ctor");
  }

  A(A& x) {
    n = x.n;
    i = id++;
    counter++;
    report("copy ctor");
  }

  A(const A& x) {    
    n = x.n;
    i = id++;
    counter++;
    report("const copy ctor");
  }
  
  ~A() {
    counter--;
    report("dtor");
  };

  void msg(void) {
    report("msg");
  }
  
};

int main(void) {

  {
    vector<A> v;

    v.push_back(1);
    //v.push_back(2);
    //v.push_back(3);
    //v.push_back(4);

    for(auto& e: v)
      e.msg();

    cout << "xxx\n";

    v[0] = 10;
    v[0] = 11;

    A a(100);
    v[0] = a;

    cout << "XXX\n";

  }

  cout << "counter = " << counter << endl;

  return 0;
  
}
