#include <stdio.h>

#include <string>

class A {

  std::string _a;

 public:

  A(std::string a) : _a(std::move(a)) { printf("xxx\n"); };

  void report(void) {
    printf("_a = %s\n", _a.c_str());
  }

};

int main(void) {

  // A a1("hi");
  // a1.report();

  std::string s1 = "\n";

  for(int i = 0; i < 2; i++) {
    s1 += "bye\n";
  }
  
  A a2(std::move(s1));
  a2.report();

  printf("s1 = %s\n", s1.c_str());

  return 0;

}
