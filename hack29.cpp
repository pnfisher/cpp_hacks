#include <memory>
#include <iostream>
#include <vector>
#include <string>

using std::unique_ptr;
using std::make_unique;

class hack {

 public:
  static int counter;

  hack() { counter++; std::cout << "hack()\n"; }
  hack(const hack& h) { counter++; std::cout << "copy()\n"; }
  
  ~hack() {
    std::cout << "~hack(), counter = " << --counter << std::endl;
  }

  void report(void) {
    std::cout << "report: counter = " << counter << std::endl;
  }

  int get(void) {
    return counter;
  }

  void reset(void) {
    counter = 0;
  }

};

int hack::counter = 0;

unique_ptr<hack> hackFactory(void) {

  unique_ptr<hack> tmp = make_unique<hack>();
  tmp->report();
  return tmp;

}

int main(void) {


  // {
  //   auto hack = hackFactory();
  //   std::cout << "xxx hack->counter = " << hack->get() << std::endl;
  // }

  {
    std::vector<std::unique_ptr<hack>> v;
    //std::vector<hack> v;

    for(int i = 0; i < 3; i++) {
      v.push_back(hackFactory());
    }

    std::cout << "aaa\n";
    
    for(auto& h: v)
      std::cout << "h->counter = " << h->get() << std::endl;

  }

  std::cout << "xxx\n";

  std::cout << "tmp->counter = " << hack().get() << std::endl;

  std::cout << std::string(5, 'x') << std::endl;

  return 0;

}
  
