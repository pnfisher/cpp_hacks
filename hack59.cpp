#include <iostream>

template<typename T, typename U>
struct isSame {
  static const bool value = false;
};

template<typename T>
struct isSame<T,T> {
  static const bool value = true;
};

class A {};
class B {};

typedef A C;

int main(void) {

  std::cout
    << "A is the same as B: "
    << std::boolalpha
    << isSame<A,B>::value
    << std::endl;

  std::cout
    << "A is the same as C: "
    << std::boolalpha
    << isSame<A,C>::value
    << std::endl;

  return 0;

}
