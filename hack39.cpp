#include <iostream>
#include <cstdio>
#include <vector>
#include <memory>

using namespace std;

class BV {

  vector<int> v;
  typedef vector<int>::size_type sz_t;

 public:

  BV() { v.reserve(100); }
  BV(sz_t sz) { v.reserve(sz); }

  int at(sz_t i) { return v.at(i); }
  sz_t capacity(void) { return v.capacity(); }
  sz_t size(void) { return v.size(); }


};

void foo(void) {

  auto bv = make_unique<BV>(0x100);
  cout << "bv->size() = " << bv->size() << endl;
  cout << "bv->at(1) = " << bv->at(1) << endl;

}

int main(void) {

  foo();

  cout << "bye\n";  
  return 0;

}
