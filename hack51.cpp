#include <iostream>
#include <memory>
#include <string>
#include <map>

typedef std::unique_ptr<std::string> u_str;

int main(void) {

  std::multimap<char, u_str> map;
  // std::unique_ptr<std::string> u_str(new std::string);

  u_str s1 = u_str(new std::string("abc")),
    s2 = u_str(new std::string("def")),
    s3 = u_str(new std::string("ghi"));

  map.insert(make_pair((*s1.get())[0], std::move(s1)));
  map.insert(make_pair((*s2.get())[0], std::move(s2)));
  map.insert(make_pair((*s3.get())[0], std::move(s3)));

  for(auto& elt: map)
    std::cout << *elt.second.get() << std::endl;

  return 0;
  
}
