#include <memory>
#include <iostream>
#include <string>

using namespace std;

namespace global {
int count = 0;
}

struct A {
  int id;
  void info(string msg) {
    cout << msg << ": id = " << id << endl;
  }
  A() {
    id = global::count++;
    info("ctor");
  }
  ~A() { info("dtor"); }
};

int main(void) {

  shared_ptr<A> sp;

  cout << "xxx\n";
  sp = make_shared<A>();
  cout << "xxx\n";
  sp = make_shared<A>();
  cout << "xxx\n";

  return 0;

}
