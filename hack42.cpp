#include <cassert>
#include <condition_variable>
#include <iostream>
#include <memory>
#include <thread>
#include <vector>

class barrier
{
 protected:
  int expected_;
  mutable std::mutex mut;
  std::condition_variable barrier_cond;

 public:
  barrier()
    : expected_(0){};

  barrier(const int expected)
    : expected_(0)
  {
    set(expected);
  }

  void set(const int expected)
  {
    assert(expected_ == 0 && expected > 0);
    expected_ = expected;
  }

  virtual void go(void) { assert(0 && "illegal use of go method"); }
  virtual void wait(void) = 0;
};

class sync_barrier : public barrier
{
 public:
  sync_barrier()
    : barrier(){};
  sync_barrier(const int expected)
    : barrier(expected){};

  virtual void wait(void)
  {
    std::unique_lock<std::mutex> lk(mut);
    assert(expected_ > 0);
    if (--expected_ != 0)
      barrier_cond.wait(lk, [this] { return expected_ == 0; });
    lk.unlock();
    barrier_cond.notify_one();
  }
};

class flag_barrier : public barrier
{
  std::condition_variable go_cond;
  bool go_ = false;

 public:
  flag_barrier()
    : barrier(){};
  flag_barrier(const int expected)
    : barrier(expected){};

  virtual void wait(void)
  {
    std::unique_lock<std::mutex> lk(mut);
    assert(expected_ > 0 && !go_);
    --expected_;
    if (expected_ == 0)
      go_cond.notify_one();
    barrier_cond.wait(lk, [this] { return go_; });
    lk.unlock();
  }

  virtual void go(void)
  {
    std::unique_lock<std::mutex> lk(mut);
    assert(expected_ >= 0 && !go_);
    if (expected_ > 0)
      go_cond.wait(lk, [this] { return expected_ == 0; });
    go_ = true;
    lk.unlock();
    barrier_cond.notify_all();
  }
};

uint64_t
fib(uint32_t n)
{
  if (n == 0)
    return 0;
  if (n == 1)
    return 1;
  return fib(n - 2) + fib(n - 1);
}

void
hello(barrier* bar, int id, uint32_t n)
{
  bar->wait();
  // std::thread::id t_id = std::this_thread::get_id();
  // std::cout << t_id << ": ";
  // printf("hello %s\n", msg);
  // pid_t tid = pthread_self();
  // printf("%u: hello %s\n", tid, msg);
  printf("%d: calculating fib(%u)\n", id, n);
  printf("%d: fib(%u) = %lu\n", id, n, fib(n));
}

int
main(void)
{
  const int nthreads = 10, max_fib = 44, sleep_secs = 2, fb = 1;
  std::unique_ptr<barrier> bar = nullptr;
  std::vector<std::thread> vt;

  if (fb)
    bar = std::make_unique<flag_barrier>(nthreads);
  else
    bar = std::make_unique<sync_barrier>(nthreads);

  printf("0: starting threads\n");

  for (int i = 1; i < nthreads + 1; i++) {
    if (i == nthreads)
      std::this_thread::sleep_for(std::chrono::seconds(2));
    vt.push_back(std::thread(hello, bar.get(), i, max_fib - i));
  }

  printf("0: all threads started\n");

  if (sleep_secs) {
    printf("0: sleeping for %d seconds\n", sleep_secs);
    std::this_thread::sleep_for(std::chrono::seconds(sleep_secs));
    printf("0: slept for %d seconds\n", sleep_secs);
  }

  if (fb) {
    printf("0: telling threads to GO\n");
    bar->go();
  }

  for (auto& t : vt)
    t.join();

  printf("0: all threads finished\n");

  return 0;
}
