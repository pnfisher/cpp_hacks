#include <memory>
#include <cstdio>

class hack {

  int counter;

 public:
  hack() : counter(0) {
    printf("hack says hello, counter = %d, &counter = %p\n",
           counter,
           &counter);
  }
  ~hack() {
    counter = -1;
    printf("hack says bye, counter = %d, &counter = %p\n",
           counter,
           &counter);
  }
  void inc(void) {
    printf("hack is incrementing counter, counter = %d, &counter = %p\n",
           counter,
           &counter);
    counter++;
    printf("hack has incremented counter, counter = %d, &counter = %p\n",
           counter,
           &counter);
  }
  void dec(void) { counter--; }
  int get(void) const { return counter; }
  void report(void) const {
    printf("counter = %d, &counter = %p\n",
           counter,
           &counter);
  }

};

template<typename H>
void play(H& h) {

  h.inc();
  h.report();

}

int main(void) {

  auto uh = std::make_unique<hack>();
  auto& h = *(uh.get());

  play(h);
  h.inc();
  h.report();
  return 0;

}
