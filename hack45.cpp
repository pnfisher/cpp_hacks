#include <cassert>
#include <chrono>
#include <cstdint>
#include <iostream>
#include <vector>

#include "benchmark.hpp"

template <int DIM, typename T>
class VecOps
{
 public:
  static T dotProduct(T* a, T* b)
  {
    return *a * *b + VecOps<DIM - 1, T>::dotProduct(a + 1, b + 1);
  }
};

template <typename T>
class VecOps<1, T>
{
 public:
  static T dotProduct(T* a, T* b) { return *a * *b; }
};

template <int DIM, typename T>
inline T
dot_product(T* a, T* b)
{
  return VecOps<DIM, T>::dotProduct(a, b);
}

template <int DIM, typename T>
class SlowVec
{
  T* v;

 public:
  SlowVec();
  SlowVec(const std::vector<T>& V)
  {
    assert(V.size() == DIM);
    v = new T[DIM];
    for (int i = 0; i < DIM; i++) {
      v[i] = V[i];
    }
  }
  ~SlowVec() { delete[] v; }
  static T dotProduct(const SlowVec& v1, const SlowVec& v2)
  {
    int sum = 0;
    for (int i = 0; i < DIM; i++) {
      sum += v1.v[i] * v2.v[i];
    }
    return sum;
  }
  void inc(void)
  {
    for (int i = 0; i < DIM; i++) v[i]++;
  }
};

template <int DIM, typename T>
class FastVec
{
  T* v;

 public:
  FastVec();
  FastVec(const std::vector<T>& V)
  {
    assert(V.size() == DIM);
    v = new T[DIM];
    for (int i = 0; i < DIM; i++) {
      v[i] = V[i];
    }
  }
  ~FastVec() { delete[] v; }
  static T dotProduct(const FastVec& v1, const FastVec& v2)
  {
    return VecOps<DIM, T>::dotProduct(v1.v, v2.v);
  }
  void inc(void)
  {
    for (int i = 0; i < DIM; i++) v[i]++;
  }
};

int
main(void)
{

  const uint32_t loops = 0x1000000;
  const int vsize = 100;
  uint64_t sum = 0;
  benchmark bm;

  std::vector<int> v1, v2;
  for (int i = 0; i < vsize; i++) {
    v1.push_back(i);
    v2.push_back(i + 1);
  }

  // int a[] = {1, 2, 3}, b[] = {5, 6, 7};
  // std::cout << "dot_product<3>(a,b) = ";
  // std::cout << dot_product<3>(a, b);
  // std::cout << "\n";

  SlowVec<vsize, int> sv1(v1);
  SlowVec<vsize, int> sv2(v2);
  std::cout << "dotProduct(sv1, sv2) = ";
  // asm volatile ("calling slowvec");
  std::cout << SlowVec<vsize, int>::dotProduct(sv1, sv2);
  std::cout << "\n";

  FastVec<vsize, int> fv1(v1);
  FastVec<vsize, int> fv2(v2);
  std::cout << "dotProduct(fv1, fv2) = ";
  // asm volatile ("calling fastvec");
  std::cout << FastVec<vsize, int>::dotProduct(fv1, fv2);
  std::cout << "\n";

  bm.clear();
  bm.start();

  sum = 0;
  for (uint32_t i = 0; i < loops; i++) {
    sum += SlowVec<vsize, int>::dotProduct(sv1, sv2);
    sv1.inc();
    sv2.inc();
    __asm__ __volatile__("" ::: "memory");
  }

  bm.stop();
  std::cout << "sum = " << sum << "\n";
  bm.report();

  bm.clear();
  bm.start();

  sum = 0;
  for (uint32_t i = 0; i < loops; i++) {
    sum += FastVec<vsize, int>::dotProduct(fv1, fv2);
    fv1.inc();
    fv2.inc();
    __asm__ __volatile__("" ::: "memory");
  }

  bm.stop();
  std::cout << "sum = " << sum << "\n";
  bm.report();

  return 0;
}
