#include <iostream>
#include <vector>
#include <array>
#include <memory>

using std::cout;

template <typename T>
T max(T a, T b) {

  if (a > b)
    return a;

  return b;

}

auto mean(const std::vector<float> seq) {

  auto m = 0.0;

  for(auto n: seq) {

    m += n;

  }

  return m / seq.size();

}

std::shared_ptr<int> f(void) {

  auto p1 = std::make_shared<int>();
  auto p2 = p1;

  cout << "f: p1.use_count() = " << p1.use_count() << std::endl;

  return p1;

}

void play1(void) {

  std::vector<int> vi(10);

  for(auto i = 0; i < 10; i++) {
    cout << "&vi[" << i << "] = " << &vi[i] << std::endl;
    vi[i] = i;
  }

  vi.push_back(11);

  for(auto i = 0; i < vi.size(); i++) {
    cout << "&vi[" << i << "] = " << &vi[i] << std::endl;
    vi[i] = i;
  }

  cout << "vi[5] = " << vi[5] << std::endl;

}

void play2(void) {

  const auto size = 10;
  std::array<int, size> ai;
  auto i = 0;

  for(auto& p: ai) {
    p = i++;
  }

  cout << "ai[5] = " << ai[5] << std::endl;

}

int main(void) {

  std::vector<float> v = { 1.1, 1.1, 1.1, 1.1 };
  std::unique_ptr<int> i_ptr { new int };

  cout << "mean = " << mean(v) << std::endl;
  cout << "max of 4 and 9 is " << max(4,9) << std::endl;

  *i_ptr = 10;

  cout << "*i_ptr = " << *i_ptr << std::endl;

  int * i = i_ptr.get();
  *i_ptr = 9;

  cout << "*i = " << *i << std::endl;

  auto p1 = f();

  cout << "p1.use_count() = " << p1.use_count() << std::endl;

  //play1();
  play2();

  return 0;

}
