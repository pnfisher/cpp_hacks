#include <chrono>
#include <iostream>
#include <iomanip>

class benchmark
{
  std::chrono::time_point<std::chrono::steady_clock> _start;
  std::chrono::duration<double> _secs;
  std::chrono::steady_clock _tp;

 public:
  benchmark() { clear(); }
  void start() { _start = _tp.now(); }
  void stop() { _secs += _tp.now() - _start; }
  void clear() { _secs = std::chrono::duration<double>(0); }
  double secs() { return _secs.count(); }
  long nsecs()
  {
    return std::chrono::duration_cast<std::chrono::nanoseconds>(_secs).count();
  }
  void report(void)
  {
    std::cout << "time stats: ";
    std::cout << "nsecs = " << nsecs();
    std::cout << ", secs = " << std::setiosflags(std::ios::fixed)
              << std::setprecision(2) << secs();
    std::cout << std::endl;
    // printf("time stats: nsecs = %ld, secs = %.2f\n", nsecs(), secs());
  }
};
