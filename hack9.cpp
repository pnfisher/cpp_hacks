#include <stdio.h>
#include <assert.h>

class Hello {

 public:
  
  virtual void talk(void) = 0;

};

class HelloWorld : public Hello {

 public:

  virtual void talk(void) {
    printf("hello world\n");
  }

};

class HelloCanada : public Hello {

 public:
  
  virtual void talk(void) {
    printf("hello Canada\n");
  }

};

int main(void) {

  Hello * h = new HelloCanada {};
  HelloWorld * hw;
  HelloCanada * hc;

  h = dynamic_cast<HelloCanada*>(h);

  assert(h);
  h->talk();

  return 0;

}
