#include <map>
#include <unordered_map>
#include <algorithm>
#include <string>

typedef std::unordered_map<int, std::string> MAP;

int main(void) {

  MAP uomap {
    { 1, "phil" },
    { 2, "katie" },
    { 3, "lizzy" },
    { 4, "jane" },
    { 5, "george" }
  };

  auto who = uomap.find(3);
  if (who != uomap.end())
    printf("who is %s\n", who->second.c_str());

  uomap[6] = "angus";

  auto w = uomap[4];
  if (!w.empty())
    printf("w = %s\n", w.c_str());

  return 0;

}
