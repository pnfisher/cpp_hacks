#include <iostream>

class Foo {

 protected:

  int64_t foo;

 public:

  Foo() { foo = 0; }
  Foo(int64_t f) { foo = f; }
  // virtual int64_t Bar() { return foo; }
  int64_t Bar() { return foo; }

};

class Foo2 : public Foo {

 public:

  int64_t Bar() { return foo; }

};

int main(void) {

  std::cout << "sizeof(Foo) = " << sizeof(Foo) << std::endl;
  std::cout << "sizeof(Foo2) = " << sizeof(Foo2) << std::endl;

  return 0;

}
