#include <iostream>

class Base {

 public:

  void hello(void) { do_hello(); }
  // virtual ~Base() { std::cout << "Base dtor\n"; };
  // ~Base() { std::cout << "Base dtor\n"; };

 protected:
  ~Base() { std::cout << "Base dtor\n"; };

 private:
  virtual void do_hello(void) { std::cout << "Hello from Base\n"; }

};

#if 1
class Derived : public Base {

 public:

  ~Derived() { std::cout << "Derived dtor\n"; };

 private:

  void do_hello(void) { std::cout << "Hello from Derived\n"; }

};
#endif

int main(void) {

  {
    // Base * B = new Base;
    // B->hello();
    // delete B;
    Derived D;
    D.hello();
  }

#if 0
  {
    Base * B = new Derived;
    B->hello();
    delete B;
  }
#endif

}
