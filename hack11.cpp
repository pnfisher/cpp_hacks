#include <cstdio>
#include <array>

#include "benchmark.hpp"

uint64_t fib(uint32_t n) {

  if (n == 0) return 0;
  if (n <= 2) return 1;
  return fib(n-2) + fib(n-1);

}

int main(void) {

  const uint32_t loops = 43;
  std::array<uint64_t, loops> fibs;
  std::array<double, loops> secs;
  uint64_t n = 0;
  benchmark bm1, bm2;

  for(uint32_t i = 1; i <= loops; i++) {
    bm1.start(); bm2.start();
    fibs[i-1] = fib(i);
    bm1.stop(); bm2.stop();
    secs[i-1] = bm2.secs();
    bm2.clear();
  }

  for(uint32_t i = 1; i <= loops; i++) {
    printf("fib(%d) %ld, secs = %f\n", i, fibs[i-1], secs[i-1]);
    n += fibs[i-1];
  }

  printf("total = %ld, secs = %f\n", n, bm1.secs());

  return 0;

}
