#include <cassert>
#include <cstdint>
#include <iostream>
#include <vector>

#include "benchmark.hpp"

#define unlikely(x) __builtin_expect((x), 0)

using namespace std;

template <uint64_t n>
struct factorial
{
  enum { ret = factorial<n - 1>::ret * n };
};

template <>
struct factorial<0>
{
  enum { ret = 1 };
};

template <uint64_t n>
struct fib
{
  static const uint64_t ret = fib<n - 2>::ret + fib<n - 1>::ret;
  // enum { ret = fib<n-2>::ret + fib<n-1>::ret };
};

template <>
struct fib<0>
{
  static const uint64_t ret = 0;
  // enum { ret = 0 };
};

template <>
struct fib<1>
{
  static const uint64_t ret = 1;
  // enum { ret = 1 };
};

class Fib
{
  static const uint32_t max_fib = 93;
  uint64_t fib_a[max_fib + 1];
  uint32_t size = 0;

  void calc_a(uint32_t n)
  {

    assert(n >= size);
    assert(n <= max_fib);

    if (n > size)
      calc_a(n - 1);

    unsigned long long res = 0;
    if (__builtin_uaddll_overflow(fib_a[n - 1], fib_a[n - 2], &res)) {
      cerr << "error, overflow with n = " << n << endl;
      throw("fib overflow");
    }
    else {
      fib_a[size++] = res;
    }
  }

 public:
  Fib()
  {
    fib_a[size++] = 0;
    fib_a[size++] = 1;
    for (; size <= max_fib; size++)
      fib_a[size] = fib_a[size - 1] + fib_a[size - 2];
    // calc_a(max_fib);
  }

  uint64_t operator()(uint32_t n) const
  {
    if (unlikely(n > max_fib))
      return (uint64_t)-1;
    // if (unlikely(n >= size))
    //   calc_a(n);
    return fib_a[n];
  }

  uint64_t operator[](uint32_t n) const { return fib_a[n]; }
};

int
main(void)
{
  benchmark bm;
  const uint64_t n = 93;
  uint64_t sum = 0;

  bm.clear();
  bm.start();

  // cout << "7! = 0x" << hex << factorial<n>::ret << endl;  // 5040
  for (uint32_t i = 0; i < 0x80000000; i++) {
    sum += fib<n>::ret;
    __asm__ __volatile__ ("" ::: "memory");
  }

  bm.stop();
  cout << "sum = " << sum << endl;
  bm.report();

  sum = 0;

  Fib f;
  bm.clear();
  bm.start();

  // cout << "7! = 0x" << hex << factorial<n>::ret << endl;  // 5040
  for (uint32_t i = 0; i < 0x80000000; i++) {
    // sum += f.get(n);
    sum += f(n);
    // sum += f[n];
    __asm__ __volatile__ ("" ::: "memory");
  }

  bm.stop();
  cout << "sum = " << sum << endl;
  bm.report();

  return 0;
}
