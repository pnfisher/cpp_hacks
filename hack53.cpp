#include <iostream>
#include <vector>

struct Thingy {

  std::vector<std::string *> m_v;

  // Thingy() = delete;

  void push_back(const std::string& s) {
    m_v.push_back(new std::string(s));
  }

  void yuck(void) {
    throw std::runtime_error("yuck");
  }

  ~Thingy() {
    std::cout << "~Thingy() called\n";
    for_each(m_v.begin(), m_v.end(), [](std::string * p) {
        std::cout << "xxx " << p << std::endl;
        delete p;
      });
  }

};

void prog(void) {

  Thingy t;

  t.push_back("abc");
  t.push_back("def");
  t.push_back("ghi");
  t.yuck();

}

int main(void) {

  try {
    prog();
  }
  catch(...) {
    return EXIT_FAILURE;
  }

  return 0;

}
