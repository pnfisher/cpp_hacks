#include <iostream>
#include <string>
#include <regex>
#include <algorithm>

int main(void) {

  std::string s = "abcdefgh";

  std::string::size_type n = s.find("g");
  if (n == std::string::npos)
    std::cout << "not found\n";
  else
    std::cout << "found " << s[n] << " at pos " << n << std::endl;

  {

    char * cs = (char *)"the rain in spain falls mainly on the plain";
    std::regex regex("(sp|pl)(ain)", std::regex::extended);
    // std::regex regex(".*", std::regex::extended);
    std::cmatch match;

    while (std::regex_search(cs, match, regex) && match.length()) {
      std::cout << "match = ";
      std::cout << match.str();
      std::cout << std::endl;
      cs = cs + match.position() + match.length();
    }
    
  }

  {

    s = "the rain in spain falls mainly on the plain";
    std::regex regex("(sp|pl)(ain)", std::regex::extended);
    std::smatch match;
    std::string tmp = s;

    while (std::regex_search(tmp, match, regex) && match.length()) {
      std::cout << "match = ";;
      std::cout << match[0];;
      std::cout << std::endl;
      tmp = match.suffix().str();
    }

  }

  // Doesn't appear to work with either g++-6 or clang++
  // {
  //   s = "the rain in spain falls mainly on the plain";
  //   std::regex regex("(sp|pl)(ain)", std::regex::extended);
  //   std::smatch match;
  //   while (std::regex_match(s.cbegin() + match.position() + match.length(),
  //                           s.cend(),
  //                           match,
  //                           regex)) {
  //     std::cout << "xxx match = ";;
  //     std::cout << match[0];;
  //     std::cout << std::endl;
  //   }
  // }

  {

    s = "the rain in spain falls mainly on the plain";
    // std::regex regex("(sp|pl)(ain)", std::regex::extended);
    std::regex regex(".*", std::regex::extended);
    std::smatch match = *std::sregex_iterator();

    for(auto i = std::sregex_iterator(s.begin(), s.end(), regex),
          end = std::sregex_iterator();
        i != end;
        i++) {
      match = *i;
      std::cout << "match = ";;
      std::cout << match[0];;
      std::cout << std::endl;
    }

  }

  return 0;

}
