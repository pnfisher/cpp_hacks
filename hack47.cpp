#include <iostream>

template <uint64_t n>
struct fib
{
  static const uint64_t ret = fib<n - 2>::ret + fib<n - 1>::ret;
  // enum { ret = fib<n-2>::ret + fib<n-1>::ret };
};

template <>
struct fib<0>
{
  static const uint64_t ret = 0;
  // enum { ret = 0 };
};

template <>
struct fib<1>
{
  static const uint64_t ret = 1;
  // enum { ret = 1 };
};

int main(int argc, char *argv[])
{

  const uint64_t n = 93;
  printf("fib(%lu) = %lu\n", n, fib<n>::ret);
  
  return 0;
}
