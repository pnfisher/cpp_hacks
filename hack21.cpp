#include <iostream>
#include <string>
#include <functional>

using std::string;

class FO
{
  const string msg_ = "hello";

 public:
  FO(){};
  FO(const string& msg)
    : msg_(msg)
  {
  }
  void operator()(void) { std::cout << msg_ << std::endl; }
};

typedef void (*FUNC) (void);

void bar(FUNC func) {

  int x = 5;
  
  func();

}

std::function<void(int)> foo(int x) {

  return (
      [=] (int y) {
        std::cout << x << " + " << y << " = " << x+y << std::endl;
      });

}

int
main(void)
{
  FO fo1;
  fo1();

  FO fo2("hi");
  fo2();

  int x = 10, y;

  auto func = foo(5);
  func(5);
  func = foo(20);
  func(20);

  foo(1)(1);

  for(int i = 0; i < 10; i++)
    func(i);

  std::pair<int, int> p1{1,2};
  std::pair<int, int> p2{3,4};

  std::tie(x,y) = p1;
  std::cout << x << " + " << y << " = " << x + y << std::endl;
  std::tie(x,y) = p2;
  std::cout << x << " + " << y << " = " << x + y << std::endl;

  return 0;
}
