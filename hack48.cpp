#include <iostream>
#include <limits>
#include <cstdint>

int main (void) {

  // volatile unsigned x = 1;
  // volatile unsigned y = 33;
  // volatile unsigned result = x << y;
  // std::cout << "Bad shift: " << result << std::endl;

  // volatile int x1 = 1;
  // volatile int result = (x1 << 31);
  // std::cout << "Bad: " << result << std::endl;

  volatile int32_t x = std::numeric_limits<int32_t>::min();
  volatile int32_t y = 7;
  volatile int32_t result = x >> y;
  std::cout << "Arithmetic shift: 0x" << std::hex << result << std::endl;

  return 0;

}

