extern "C" int atoi(const char *);

int main(int argc, char **argv) {

  if (argc != 3)
    return -1;

  return atoi(argv[1]) + atoi(argv[2]);

}
