#include <iostream>
#include <algorithm>
#include <vector>
#include <list>
#include <deque>

// typedef std::vector<int> C;
// typedef std::list<int> C;
typedef std::deque<int> C;

// template<typename T>
// void print(T& t) {
//   for(auto elt: t)
//     std::cout << elt << " ";
//   std::cout << std::endl;
// }

template<typename T>
void print(T& t) {
  // for_each(t.begin(),
  //          t.end(),
  //          [](auto elt) {
  //            std::cout << elt << " "; }
  //          );
  for(typename T::value_type elt: t)
    std::cout << elt << " ";
  std::cout << std::endl;
}

int main(void) {

  C c1;

  for(int i = 0; i < 10; i++)
    c1.push_back(i);

  print(c1);
  auto end = std::remove_if(c1.begin(), c1.end(), [](int i) { return i < 5; });
  print(c1);
  c1.erase(end, c1.end());
  print(c1);

  return 0;

}
