#include <cassert>
#include <cstdint>
#include <iostream>
#include <vector>

using namespace std;

template <typename T>
const T
adder(const T& t)
{
  return t;
}

template <typename T, typename... Args>
const T
adder(const T& t, Args... args)
{
  return t + adder(args...);
}

int
main(void)
{
  uint64_t sum = adder(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
  cout << "sum = " << sum << endl;

  return 0;
}
