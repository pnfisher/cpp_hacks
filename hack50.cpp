#include <iostream>
#include <fstream>
#include <cassert>

void usage(void) {

  std::cerr << "usage:" << std::endl;
  std::cerr << "  hack50: min max file_in file_out" << std::endl;

}

int main (int argc, char ** argv) {

  if (argc != 5) {
    std::cerr << "error, incorrect arguments" << std::endl;
    usage();
    exit(1);
  }

  char ch;
  std::ifstream fin(argv[3]);
  assert(fin.is_open());

  while (fin.get(ch))
    std::cout << ch;

  return 0;

}
