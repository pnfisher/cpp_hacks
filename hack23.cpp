#include <iostream>
#include <functional>

class Cleaner {

  typedef std::function<void(void)> CleanFunc;
  CleanFunc func;

 public:

  //Cleaner() { std::cout << "ctor\n"; }
  // Cleaner(Cleaner& rhs) {
  //   func = rhs.func;
  //   rhs.func = 0;
  // }
  // Cleaner& operator+(std::function<void(void)> fn) {
  //   //return Cleaner(fn);
  //   func = fn;
  //   return *this;
  // }
  // std::function<void(void)> operator+(std::function<void(void)> fn) {
  //   return fn;
  // }
  Cleaner(CleanFunc fn) : func(fn) { std::cout << "cp ctor\n"; }
  ~Cleaner() {
    if (func)
      func();
  }

};


// #define SCOPE_EXIT \
//   Cleaner xxx = Cleaner() + [&]()
#define SCOPE_EXIT \
  Cleaner xxx = (Cleaner)[&]()



int main(void) {

  int x = 0;

  {
    SCOPE_EXIT { std::cout << "xxx\n"; };
    std::cout << "bye\n";
  }

  std::cout << "bye again\n";

  return 0;

}
