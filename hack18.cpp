#include <iostream>
#include <vector>

using namespace std;

struct TV {

  static const int size = 3;

  vector<int> v1;
  vector<int> v2;

  TV() {
    for(int i = 0; i < size; i++) {
      v1.push_back(i);
      v2.push_back(i+size);
    }
  }

  void report(string msg = "xxx") {
    cout << msg << endl;
    cout << "v1 = ";
    for(auto i: v1)
      cout << i << " ";
    cout << "\nv2 = ";
    for(auto i: v2)
      cout << i << " ";
    cout << "\n";
  }

  void bump(void) {
    for(auto& i: v1)
      i++;
  }
  
};


int main(void) {

  TV tv1;
  tv1.report();
  tv1.bump();
  tv1.report();

  TV tv2 = tv1;
  tv2.report();
  tv2.bump();

  TV tv3(tv2);
  tv3.report();

  return 0;

}
