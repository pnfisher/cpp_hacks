#include <stdio.h>

static int counter = 10;

int foo(void) {

  counter++;
  printf("-> ");
  return counter;

}

int main(void) {

  printf("hello world: counter = %d\n", foo());
  return 0;

}
