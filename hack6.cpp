#include <stdio.h>
#include <assert.h>
#include <string.h>

#include <iterator>
#include <cstdint>
#include <ctime>

int main(void) {

  const uint32_t loops = 0x8000000;
  uint32_t count = 0;
  int rc = 0;
  
  struct timespec start, stop;
  uint64_t nsecs = 0;
  for (uint32_t i = 0; i < loops; i++) {

    uint32_t shift = i % 32;

    rc = clock_gettime(CLOCK_MONOTONIC, &start);
    assert(!rc);

    //if (((i >> shift) & 0x1) != 0)
    //  count++;

    if ((i & (1 << shift)) != 0)
      count++;

    rc = clock_gettime(CLOCK_MONOTONIC, &stop);
    assert(!rc);

    nsecs += (stop.tv_sec * 1e9) + stop.tv_nsec;
    nsecs -= (start.tv_sec * 1e9) + start.tv_nsec;

  }

  printf("time = %.5f\n", nsecs/1e9);
  return 0;

}
