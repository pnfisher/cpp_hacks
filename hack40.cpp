#include <iostream>
#include <cstdio>
#include <vector>

using namespace std;

class Person {

  string name_ = "unknown";
  uint32_t age_ = 0;

 public:

  Person() {}
  Person(string name, uint32_t age) : name_(name), age_(age) {}
  string& name(void) { return name_; }
  uint32_t& age(void) { return age_; }

  friend ostream& operator<<(ostream& os, const Person& p) {
    os << p.name_ << ", " << p.age_;
    return os;
  }

};


int main(void) {

  vector<Person> v {
    { "Phil", 29 },
    { "Jane", 16 },
    { "Lizzy", 16 },
    { "Katie", 25 },
    { "George", 8 }
  };

  int count = 0;
  for (auto i: v) {
    count++;
    cout << i << endl;
  }

  Person p;

  cout << p.name() << " is " << p.age() << endl;

  return 0;

}
