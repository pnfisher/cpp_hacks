#include <iostream>

class Point {

 private:
  int x;
  int y;

 public:

  Point() { std::cout << "ctor\n"; }
  Point(const Point& rhs) {
    x = rhs.x;
    y = rhs.y;
    std::cout << "copy ctor\n";
  }
  Point(int _x, int _y) : x(_x), y(_y) {
    std::cout << "x,y ctor\n";
  }
  Point& operator=(const Point& rhs) {
    x=rhs.x;
    y=rhs.y;
    std::cout << "op=\n";
    return *this;
  }
  
  void report() {
    std::cout << "x = " << x << ", y = " << y << std::endl;
  }
};

Point make_point(int x, int y) {
  return { x, y };
}

int main(void) {

  Point x = make_point(1,2);
  x.report();

  Point y { 2, 1 };
  y.report();

  Point z = x;
  z.report();

  return 0;

}
