#include <iostream>
#include <limits>
#include <cstdint>

template<typename T>
struct foo {

  static T sdm;
  
};

template<typename T>
T foo<T>::sdm = 10;

int main (void) {

  std::cout << "foo::sdm = " << foo<int>::sdm << std::endl;

  return 0;

}

