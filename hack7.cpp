#include <vector>
#include <cstdint>
#include <string>

#include <assert.h>
#include <stdio.h>
#include <time.h>

class BM {

 private:
  struct timespec _start, _stop;
  uint64_t nsecs;
  
 public:
  BM() : nsecs(0) { };
  void start(void) {
    int rc = clock_gettime(CLOCK_MONOTONIC, &_start);
    assert(!rc);
  };
  void stop(void) {
    int rc = clock_gettime(CLOCK_MONOTONIC, &_stop);
    assert(!rc);
    nsecs += (_stop.tv_sec * 1e9) + _stop.tv_nsec;
    nsecs -= (_start.tv_sec * 1e9) + _start.tv_nsec;
  };
  void reset(void) { nsecs = 0; };
  void report(char * msg = 0, int decimals = 5) {
    if (msg)
      fprintf(stdout, "time stats for %s\n", msg);
    fprintf(stdout, "total = %ld (nanosecs)\n", nsecs);
    fprintf(stdout, "time  = %.*f (secs)\n", decimals, nsecs/1e9);
  };

};

#define ESCAPE(p) \
  asm volatile( \
      "" \
      : \
      : "g"((void*)p) \
      : "memory")

#define CLOBBER()  \
  asm volatile( \
      "" \
      : \
      : \
      : "memory")

void escape(void * p) {
  ESCAPE(p);
  asm volatile("nop");
}

void clobber(void) {
  CLOBBER();
  asm volatile("nop");
}

// __attribute__((optnone))
// void reserve_w(std::vector<int>& v) {
//   v.reserve(1);
// }

// __attribute__((optnone))
// void push_back_w(std::vector<int>& v) {
//   v.push_back(0xdeadbeef);
// }

template<typename T>
__attribute__((optnone))
void reserve(T& v) {
  v.reserve(1);
}

template<typename T, typename U>
__attribute__((optnone))
void pushback(T& v, U i) {
  v.push_back(i);
}


__attribute__((noinline))
void pushback(uint32_t loops) {
  for (uint32_t i = 0; i < loops; i++) {
    std::vector<int> v;
    v.reserve(1);
    //reserve(v);
    asm volatile("nop" : : "g"(v.data()) : "memory");
    //reserve_w(v);
    //escape((void *)v.data());
    //asm volatile("nop" : : "g"(v.data()) : "memory");
    //asm volatile("nop" : "+m"((int&)(*v.data())) : : "memory");
    //ESCAPE(v.data());
    //asm volatile("nop" : : "g"((void *)&v) : "memory");
    v.push_back(0x4242);
    //pushback(v, 0x4242);
    //push_back_w(v);
    //asm volatile("" : : "g"(p) : "memory");
    //asm volatile("nop" : "+m"(p) : : "memory");
    //CLOBBER();
    //clobber();
    asm volatile("nop" : : : "memory");
  }
}

int main (void) {

  uint32_t loops = 100000000;
  BM bm;

  // bm.reset();
  // bm.start();
  // reserve(loops);
  // bm.stop();
  // bm.report((char *)"reserve");

  bm.reset();
  bm.start();
  pushback(loops);
  bm.stop();
  bm.report((char *)"push_back");

}
