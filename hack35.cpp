#include <vector>
#include <cstdint>
#include <iostream>
#include <cassert>

using namespace std;

class fib {

  static const uint32_t max_fib = 93;
  vector<uint64_t> fibs;

  void calc(uint32_t n) {

    assert(n >= fibs.size());
    assert(n <= max_fib);

    if (n > fibs.size())
      calc(n-1);

    unsigned long long res = 0;
    if (__builtin_uaddll_overflow(fibs[n-1], fibs[n-2], &res)) {
      cerr << "error, overflow with n = " << n << endl;
      throw("fib overflow");
    }
    else {
      fibs.push_back(res);
    }

  }

 public:

  fib() {
    fibs.push_back(0);
    fibs.push_back(1);
    fibs.push_back(1);
  }

  uint64_t get(uint32_t n) {

    if (n > max_fib)
      return (uint64_t)-1;

    if (n >= fibs.size())
      calc(n);
    return fibs[n];

  }

};

int main(void) {

  const uint32_t n = 11;
  fib f;

  cout << "fib(" << n << ") = 0x" << hex << f.get(n) << endl;

  return 0;

}
