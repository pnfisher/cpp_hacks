#include <iostream>

struct A {
  int a;
  void noop(void) { std::cout << "noop\n"; }
  // A() { std::cout << "A()\n"; }
};

struct Base {

  Base() { std::cout << "Base()\n"; }
  ~Base() { std::cout << "~Base()\n"; }

};

struct Derived : public Base {

  Derived() { std::cout << "Derived()\n"; }
  ~Derived() { std::cout << "~Derived()\n"; }

};

int main(void) {

  Derived d;

  if (std::is_pod<A>::value)
    std::cout << "A is POD\n";
  if (std::is_pod<Base>::value)
    std::cout << "Base is POD\n";
  if (std::is_pod<Derived>::value)
    std::cout << "Derived is POD\n";

  return 0;

}
