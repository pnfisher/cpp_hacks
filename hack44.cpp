#include <iostream>

template <typename T>
struct isChar
{
  enum { value = 0 };
};

template <>
struct isChar<char>
{
  enum { value = 1 };
};

template <>
struct isChar<unsigned char>
{
  enum { value = 1 };
};

template <typename T>
struct NoCV
{
  // typedef T type;
  using type = T;
};

template <typename T>
struct NoCV<const T>
{
  // typedef T type;
  using type = T;
};

template <typename T>
struct NoCV<volatile T>
{
  // typedef T type;
  using type = T;
};

template <typename T>
struct isAnyChar
{
  // enum { value = isChar<typename NoCV<T>::type>::value };
  enum { value = isChar<typename NoCV<T>::type>::value };
};

int
main(int argc, char* argv[])
{
  std::cout << "isChar<int>::value = ";
  std::cout << isChar<int>::value;
  std::cout << "\n";

  std::cout << "isChar<char>::value = ";
  std::cout << isChar<char>::value;
  std::cout << "\n";

  std::cout << "isChar<const char>::value = ";
  std::cout << isChar<const char>::value;
  std::cout << "\n";

  std::cout << "isAnyChar<const char>::value = ";
  std::cout << isAnyChar<const char>::value;
  std::cout << "\n";

  std::cout << "isAnyChar<const unsigned char>::value = ";
  std::cout << isAnyChar<const unsigned char>::value;
  std::cout << "\n";

  return 0;
}
