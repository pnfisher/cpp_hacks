#include <cstdio>
#include <vector>
#include <array>
#include <numeric>

#include "benchmark.hpp"

inline
void pstats(uint32_t total, benchmark& bm) {
  printf("total = 0x%x, secs = %f, nsecs = %ld\n",
         total,
         bm.secs(),
         bm.nsecs());
}

template<typename T>
__attribute__((noinline))
void sum(T& v) {

  benchmark bm;
  uint32_t total;

  bm.clear();
  bm.start();
  total = 0;
  for(auto it = v.begin(), end = v.end(); it != end; it++) {
    total = total + *it;
  }
  bm.stop();
  pstats(total, bm);

  bm.clear();
  bm.start();
  total = 0;
  for(auto i: v) {
    total += i;
  }
  bm.stop();
  pstats(total, bm);

  bm.clear();
  bm.start();
  total = 0;
  for(uint32_t i = 0, sz = v.size(); i < sz; i++) {
    total += v[i];
  }
  bm.stop();
  pstats(total, bm);

  bm.clear();
  bm.start();
  asm volatile ("nop");
  total = std::accumulate(v.begin(), v.end(), 0);
  bm.stop();
  pstats(total, bm);

  bm.clear();
  bm.start();
  uint32_t t = 0;
  auto __first = v.begin(), __last = v.end();
  asm volatile ("nop");
  for (; __first != __last; ++__first) {
    t = t + *__first;
  }
  bm.stop();
  pstats(t, bm);

}

int main(void) {

  const uint32_t loops = 0x80000000;
  auto v = new std::vector<uint32_t>;

  v->reserve(loops);

  for(uint32_t i = 0; i < loops; i++) {
    v->push_back(i);
  }

  sum(*v);
  delete v;

  auto a = new std::array<uint32_t, loops>;
  for(uint32_t i = 0; i < loops; i++) {
    (*a)[i] = i;
  }

  sum(*a);

  return 0;

}
