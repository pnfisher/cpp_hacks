#include <iostream>
#include <iterator>
#include <algorithm>

class CountFrom {
 private:
  int &count;
 public:
  CountFrom(int &n) : count(n) {}
  int operator()(void) { return count++; }
};

int main()
{
  int state(20);
  std::generate_n(std::ostream_iterator<int>(std::cout, "\n"),
                  10,
                  CountFrom(state));
  return 0;
}
