#include <iostream>

constexpr long long fib(long long n) {

  return n == 0 ? 0 : (n <= 2 ? 1 : fib(n-2)+ fib(n-1));

}

int main(void) {

  //const long long n = 50;

  std::cout << "xxx" << std::endl;
  std::cout << std::flush;
  
  std::cout << "fib = " << fib(40) << std::endl;
  return 0;

}
