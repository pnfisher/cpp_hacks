#include <cassert>
#include <cstdint>
#include <cstdio>
#include <iostream>
#include <vector>

#include "benchmark.hpp"

template <typename E, typename T>
class VecExpression
{
 public:
  T operator[](uint32_t i) const { return static_cast<E const&>(*this)[i]; }
  uint32_t size() const { return static_cast<E const&>(*this).size(); }

  // The following overloads conversions to E, the template argument type;
  // e.g., for VecExpression<VecSum>, this is a conversion to VecSum.
  operator E&()
  {
    // std::cout << "bink\n";
    return static_cast<E&>(*this);
  }
  operator E const&() const
  {
    // std::cout << "bonk\n";
    return static_cast<const E&>(*this);
  }
};

template <typename T>
class Vec : public VecExpression<Vec<T>, T>
{
  std::vector<T> elems;

 public:
  T operator[](uint32_t i) const { return elems[i]; }
  T& operator[](uint32_t i) { return elems[i]; }
  uint32_t size() const { return elems.size(); }

  Vec(uint32_t n)
    : elems(n)
  {
  }

  // A Vec can be constructed from any VecExpression, forcing its evaluation.
  template <typename E>
  Vec(VecExpression<E, T> const& vec)
    : elems(vec.size())
  {
    for (uint32_t i = 0, sz = vec.size(); i < sz; ++i) {
      elems[i] = vec[i];
    }
  }

  void inc()
  {
    for (uint32_t i = 0, sz = elems.size(); i < sz; ++i) {
      elems[i] += 1;
    }
  }
};

template <typename E1, typename E2, typename T>
class VecSum : public VecExpression<VecSum<E1, E2, T>, T>
{
  E1 const& _u;
  E2 const& _v;

 public:
  VecSum(VecExpression<E1, T> const& u, VecExpression<E2, T> const& v)
    : _u(u)
    , _v(v)
  {
    // assert(u.size() == v.size());
  }

  T operator[](uint32_t i) const { return _u[i] + _v[i]; }
  uint32_t size() const { return _v.size(); }
};

template <typename E1, typename E2, typename T>
VecSum<E1, E2, T> const
operator+(VecExpression<E1, T> const& u, VecExpression<E2, T> const& v)
{
  return VecSum<E1, E2, T>(u, v);
}

template <typename T>
class VecN
{
  std::vector<T> elems;

 public:
  VecN(uint32_t n)
    : elems(n)
  {
  }

  auto begin(void) -> decltype(elems.begin()) { return elems.begin(); }
  auto end(void) -> decltype(elems.begin()) { return elems.end(); }
  T& operator[](uint32_t i) { return elems[i]; }
  T operator[](uint32_t i) const { return elems[i]; }
  uint32_t size() const { return elems.size(); }

  void inc()
  {
    for (uint32_t i = 0, sz = elems.size(); i < sz; i++) {
      elems[i] += 1;
    }
  }

  VecN& operator+=(const VecN& v)
  {
    for (uint32_t i = 0, sz = elems.size(); i < sz; i++) {
      elems[i] += v[i];
    }
    return *this;
  }

  // VecN& operator+=(VecN& v) {
  //   for(auto eb = elems.begin(), ee = elems.end(),
  //            vb = v.begin(), ve = v.end();
  //       eb < ee;
  //       eb++, vb++) {
  //     *eb += *vb;
  //   }
  //   return *this;
  // }
};

template <typename T>
VecN<T>
operator+(const VecN<T>& u, const VecN<T>& v)
{
  const uint32_t sz = u.size();
  // assert(sz == v.size());
  VecN<T> sum(sz);
  for (uint32_t i = 0; i < sz; i++) {
    sum[i] = u[i] + v[i];
  }
  return sum;
}

template <typename T>
VecN<T>&&
operator+(VecN<T>&& u, const VecN<T>& v)
{
  for (uint32_t i = 0, sz = u.size(); i < sz; i++) {
    u[i] = u[i] + v[i];
  }
  return std::move(u);
}

template <typename T>
void
test(const uint32_t sz, const uint32_t loops)
{
  benchmark bm;
  T v(sz);

  for (uint32_t i = 0; i < sz; i++)
    v[i] = i;

  T v1(v);
  T v2(v);
  T v3(v);
  T v4(v);
  T v5(v);
  T v6(v);
  T v7(v);
  T v8(v);
  T v9(v);
  T v10(v);

  bm.clear();
  bm.start();
  uint64_t sum = 0;
  for (uint32_t i = 0; i < loops; i++) {
    T x = v1 + v2 + v3 + v4 + v5 + v6 + v7 + v8 + v9 + v10;
    for (uint32_t j = 0; j < sz; j++)
      sum += x[j];
    v1.inc();
  }
  bm.stop();

  printf("sum = %ld\n", sum);
  bm.report();
}

template <typename V>
inline void
addV(V& sum, V const& u, V const& v)
{
  for (uint32_t i = 0, sz = u.size(); i < sz; i++) {
    sum[i] = u[i] + v[i];
  }
}

template <typename T>
void
test3(const uint32_t sz, const uint32_t loops)
{
  benchmark bm;
  T v(sz);

  for (uint32_t i = 0; i < sz; i++)
    v[i] = i;

  T v1(v);
  T v2(v);
  T v3(v);
  T v4(v);
  T v5(v);
  T v6(v);
  T v7(v);
  T v8(v);
  T v9(v);
  T v10(v);

  bm.clear();
  bm.start();
  uint64_t sum = 0;
  for (uint32_t i = 0; i < loops; i++) {
    T x(sz);
    addV(x, v1, v2);
    addV(x, x, v3);
    addV(x, x, v4);
    addV(x, x, v5);
    addV(x, x, v6);
    addV(x, x, v7);
    addV(x, x, v8);
    addV(x, x, v9);
    addV(x, x, v10);
    for (uint32_t j = 0; j < sz; j++)
      sum += x[j];
    v1.inc();
  }
  bm.stop();

  printf("sum = %ld\n", sum);
  bm.report();
}

template <typename T>
inline auto
adder(uint32_t i, const T& t) -> decltype(t[0])
{
  return t[i];
}

template <typename T, typename... Args>
inline auto
adder(uint32_t i, const T& v, Args... args) -> decltype(v[0])
{
  return v[i] + adder(i, args...);
}

template <typename S, typename U, typename V>
inline void
adderV(S& sum, const U& u, const V& v)
{
  const uint32_t sz = u.size();
  for (uint32_t i = 0; i < sz; i++) {
    sum[i] = u[i] + v[i];
  }
}

template <typename S, typename U, typename V, typename... Vs>
inline void
adderV(S& sum, const U& u, const V& v, const Vs&... vs)
{
  adderV(sum, u, v);
  // assert(sizeof...(vs) > 0);
  adderV(sum, sum, vs...);
}

template <typename V>
inline void
adderVV(
  V& sum,
  const V& v1,
  const V& v2,
  const V& v3,
  const V& v4,
  const V& v5,
  const V& v6,
  const V& v7,
  const V& v8,
  const V& v9,
  const V& v10)
{
  for (uint32_t i = 0, sz = sum.size(); i < sz; i++) {
    sum[i] = v1[i] + v2[i] + v3[i] + v4[i] + v5[i] + v6[i] + v7[i] + v8[i] +
             v9[i] + v10[i];
    // sum[i] = 0;
    // sum[i] += v1[i];
    // sum[i] += v2[i];
    // sum[i] += v3[i];
    // sum[i] += v4[i];
    // sum[i] += v5[i];
    // sum[i] += v6[i];
    // sum[i] += v7[i];
    // sum[i] += v8[8];
    // sum[i] += v9[i];
    // sum[i] += v10[i];
  }
}

template <typename T>
void
test4(const uint32_t sz, const uint32_t loops)
{
  benchmark bm;
  T v(sz);

  for (uint32_t i = 0; i < sz; i++)
    v[i] = i;

  T v1(v);
  T v2(v);
  T v3(v);
  T v4(v);
  T v5(v);
  T v6(v);
  T v7(v);
  T v8(v);
  T v9(v);
  T v10(v);

  bm.clear();
  bm.start();
  uint64_t sum = 0;
  for (uint32_t i = 0; i < loops; i++) {
    T x(sz);
    adderV(x, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10);
    for (uint32_t j = 0; j < sz; j++)
      sum += x[j];
    v1.inc();
  }
  bm.stop();

  printf("sum = %ld\n", sum);
  bm.report();
}

template <typename T>
void
test5(const uint32_t sz, const uint32_t loops)
{
  benchmark bm;
  T v(sz);

  for (uint32_t i = 0; i < sz; i++)
    v[i] = i;

  T v1(v);
  T v2(v);
  T v3(v);
  T v4(v);
  T v5(v);
  T v6(v);
  T v7(v);
  T v8(v);
  T v9(v);
  T v10(v);

  bm.clear();
  bm.start();
  uint64_t sum = 0;
  for (uint32_t i = 0; i < loops; i++) {
    T x(sz);
    adderVV(x, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10);
    for (uint32_t j = 0; j < sz; j++)
      sum += x[j];
    v1.inc();
  }
  bm.stop();

  printf("sum = %ld\n", sum);
  bm.report();
}

template <typename T>
void
test6(const uint32_t sz, const uint32_t loops)
{
  benchmark bm;
  T v(sz);

  for (uint32_t i = 0; i < sz; i++)
    v[i] = i;

  T v1(v);
  T v2(v);
  T v3(v);
  T v4(v);
  T v5(v);
  T v6(v);
  T v7(v);
  T v8(v);
  T v9(v);
  T v10(v);

  bm.clear();
  bm.start();
  uint64_t sum = 0;
  for (uint32_t i = 0; i < loops; i++) {
    T x(sz);
    for (uint32_t j = 0; j < sz; j++) {
      x[j] = v1[j] + v2[j] + v3[j] + v4[j] + v5[j] + v6[j] + v7[j] + v8[j] +
             v9[j] + v10[j];
    }
    for (uint32_t j = 0; j < sz; j++)
      sum += x[j];
    v1.inc();
  }
  bm.stop();

  printf("sum = %ld\n", sum);
  bm.report();
}

template <typename T>
void
test7(const uint32_t sz, const uint32_t loops)
{
  benchmark bm;
  T v(sz);

  for (uint32_t i = 0; i < sz; i++)
    v[i] = i;

  T v1(v);
  T v2(v);
  T v3(v);
  T v4(v);
  T v5(v);
  T v6(v);
  T v7(v);
  T v8(v);
  T v9(v);
  T v10(v);

  bm.clear();
  bm.start();
  uint64_t sum = 0;
  for (uint32_t i = 0; i < loops; i++) {
    T x(v1);
    x += v2;
    x += v3;
    x += v4;
    x += v5;
    x += v6;
    x += v7;
    x += v8;
    x += v9;
    x += v10;
    for (uint32_t j = 0; j < sz; j++)
      sum += x[j];
    v1.inc();
  }
  bm.stop();

  printf("sum = %ld\n", sum);
  bm.report();
}

template <typename T>
void
test8(const uint32_t sz, const uint32_t loops)
{
  benchmark bm;
  T v(sz);

  for (uint32_t i = 0; i < sz; i++)
    v[i] = i;

  T v1(v);
  T v2(v);
  T v3(v);
  T v4(v);
  T v5(v);
  T v6(v);
  T v7(v);
  T v8(v);
  T v9(v);
  T v10(v);

  bm.clear();
  bm.start();
  uint64_t sum = 0;
  for (uint32_t i = 0; i < loops; i++) {
    T x(sz);
    x += v1;
    x += v2;
    x += v3;
    x += v4;
    x += v5;
    x += v6;
    x += v7;
    x += v8;
    x += v9;
    x += v10;
    for (uint32_t j = 0; j < sz; j++)
      sum += x[j];
    v1.inc();
  }
  bm.stop();

  printf("sum = %ld\n", sum);
  bm.report();
}

int
main(void)
{
  const uint32_t sz = 1000, loops = 0x180000;
  test<Vec<uint32_t>>(sz, loops);
  test<VecN<uint32_t>>(sz, loops);
  test3<VecN<uint32_t>>(sz, loops);
  test4<VecN<uint32_t>>(sz, loops);
  test5<VecN<uint32_t>>(sz, loops);
  test6<VecN<uint32_t>>(sz, loops);
  test7<VecN<uint32_t>>(sz, loops);
  test8<VecN<uint32_t>>(sz, loops);
  return 0;
}
