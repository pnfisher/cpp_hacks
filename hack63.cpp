#include <iostream>

class myint {

  int _i;

 public:

  myint(void) : _i(0) { std::cout << "myint: default ctor\n"; }
  myint(int i) : _i(i) { std::cout << "myint: other ctor\n"; }
  void print(void) { std::cout << "myint = " << _i << std::endl; }

};

int main(void) {

  myint m0;
  myint m1(1);
  myint m2{2};

  return 0;

}
