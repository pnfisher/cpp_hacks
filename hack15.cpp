#include <stdio.h>

#include <memory>

class A {

  int a;

 public:
  
  A() : a(0) { printf("A()\n"); }
  A(A& in) : a(in.a) { printf("A(A& in)\n"); }
  A(A&& in) : a(in.a) { printf("A(A&& in)\n"); }
  A(int in) : a(in) { printf("A(int in)\n"); }

  A& operator=(A in) {
    a = in.a;
    printf("op=\n");
    return *this;
  }

  int get(void) { return a; }
  
};

A foo() {
  A a;
  return std::move(a);
}

int main(void) {

  printf("1\n");
  A a1 { 3 };
  printf("2\n");
  A a2 = a1;
  printf("3\n");
  A a3;
  printf("4\n");
  a3 = a2;
  printf("5\n");
  A a4 = foo();

  return 0;

}
