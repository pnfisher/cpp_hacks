#include <iostream>
#include <memory>

class Hello {
 public:
  virtual void greet(void) {
    std::cout << "hello" << "\n";
  }
};

class World : public Hello {
 public:
  void greet (void) {
    std::cout << "hello world" << "\n";
  }
};

class Earth : public Hello {
 public:
  void greet (void) {
    std::cout << "hello earth" << "\n";
  }
};

void greet(Hello * h) {
  h->greet();
}

void greeter(Hello * h) {
  std::cout << "going to call greet" << "\n";
  h->greet();
}

int main(int argc, char *argv[])
{
  std::unique_ptr<Hello> h0 = std::make_unique<Hello>();
  std::unique_ptr<Hello> h1 = std::make_unique<World>();
  std::unique_ptr<Hello> h2 = std::make_unique<Earth>();

  greet(h0.get());
  greet(h1.get());
  greet(h2.get());

  greeter(h0.get());
  greeter(h1.get());
  greeter(h2.get());

  return 0;
}
