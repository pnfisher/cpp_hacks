#include <iostream>
#include <memory>
#include <vector>

class Base {

 public:
  virtual void msg(void) { std::cout << "msg: Base\n"; }
  virtual ~Base() { std::cout << "dtor: Base\n"; }

};

class Derived : public Base {

 public:
  virtual void msg(void) { std::cout << "msg: Derived\n"; }
  ~Derived() { std::cout << "dtor: Derived\n"; }

};

int main(void) {

  std::vector<std::unique_ptr<Base>> v;

  for(int i = 0; i < 4; i++) {
    if (!(i & 0x1))
      v.push_back(std::make_unique<Base>());
    else
      v.push_back(std::make_unique<Derived>());
  }

  for(auto& e: v)
    e->msg();

  return 0;
}
