RELEASE = $(shell . /etc/lsb-release && echo $$DISTRIB_CODENAME)
ARCH = $(shell uname -m)

CXX = clang++
CC = $(CXX)
CXX_STD = c++1y
STDLIBPATH = /home/phil/opt/$(RELEASE)/$(ARCH)/lib
EXTRAFLAGS ?=
OPTFLAGS ?= -O0 -g
CPPFLAGS = -Wall -std=$(CXX_STD) $(OPTFLAGS) $(EXTRAFLAGS)
LDFLAGS = -L$(STDLIBPATH)
LDLIBS = -lpthread
RUN =
SRC = $(wildcard *.cpp)
PRGS = $(subst .cpp,,$(SRC))
STDLIBCPP ?= -stdlib=libc++
RTAGS ?= $(BEAR)
JSON = compile_commands.json
DEP = .depend

ifeq ($(CXX),clang++)
CPPFLAGS += $(STDLIBCPP)
LDFLAGS += $(STDLIBCPP)
endif

define compdb
rm -f $(JSON)
@echo "[" > $(JSON); \
for p in $1; do \
	echo "{"; \
	echo "  \"directory\" : \"$(CURDIR)\","; \
	echo "  \"command\" : \"$$(make -snk --no-print-directory $$p.o)\","; \
	echo "  \"file\" : \"$(CURDIR)/$$p.cpp\","; \
	echo "},"; \
done | sed "s/[ ]*$(STDLIBCPP)[ ]*/ /g" >> $(JSON); \
echo "]" >> $(JSON)
@echo $(JSON) contains:
@cat $(JSON)
endef

define bear
rm -f $1.o
bear $(MAKE) STDLIBCPP= $1
endef

ifneq ($(RUN),)
#default: $(RUN)
#	LD_LIBRARY_PATH=$(STDLIBPATH) ./$(RUN)
default:
	rm -f $(RUN) $(RUN).o
	$(MAKE) $(RUN)
	LD_LIBRARY_PATH=$(STDLIBPATH) ./$(RUN) $(MIN) $(MAX) $(IN) $(OUT)
endif

ifneq ($(RTAGS),)
default:
ifneq ($(BEAR),)
	@$(call bear, $(RTAGS))
else
	$(call compdb, $(RTAGS))
endif
	rc -J
endif

ifneq ($(RC),)
default:
	make -snk STDLIBCPP= $(RC).o | rc -c -
endif

ifneq ($(SCAN),)
SCANFLAGS ?=
default:
	rm -f $(SCAN).o
	scan-build $(SCANFLAGS) -o scan $(MAKE) $(SCAN)
endif

error:
	$(error "missing command")

all:
	make $(PRGS)

rtags: clean
ifneq ($(BEAR),)
	@$(call bear, $(PRGS))
else
	make json
endif
	rc -J

json:
	$(call compdb, $(PRGS))

.PHONY: deps
$(DEP) deps:
	@rm -f $(DEP)
	$(CXX) $(CPPFLAGS) -MM $(SRC) > $(DEP)

-include $(DEP)

%.ll: %.cpp
	@rm -f $@
	$(CXX) $(CPPFLAGS) -S -emit-llvm $^

#%: %.cpp
#	$(CXX) $(CPPFLAGS) $(LDFLAGS) $^ -o $@ $(LDLIBS)

.PHONY: clean
clean:
	rm -f *.o *.ll $(PRGS) $(DEP)
