#include <iostream>
#include <string>

static int counter = 0;

class Who
{
  std::string who_;
  int id;

 public:
  Who() : who_("unknown") { id = counter++; }
  Who(const std::string& w) : who_(w)
  {
    std::cout << "string ctor\n";
    id = counter++;
  }
  Who(const Who& w) : who_(w.who_)
  {
    std::cout << "copy ctor\n";
    id = counter++;
  }
  Who(Who&& w)
  {
    std::cout << "move ctor\n";
    who_ = std::move(w.who_);
    // w.who_ = nullptr;
    id = counter++;
  }
  void who() { std::cout << "id = " << id << ", who = " << who_ << std::endl; }
};

Who
whoknows()
{
  //static Who w("who knows");
  Who w("who knows");
  return w;
}

int
main(void)
{
  //auto w = std::move(whoknows());
  auto w = whoknows();
  w.who();
  return 0;
}
