#include <stdio.h>

int main(int argc, char *argv[])
{

  int * var;
  const int ** constptr = &var;
  int const fixed = 20;
  *constptr = &fixed;
  *var = 30;
  printf("fixed = %i, *var = %i\n", fixed, *var);
  
  
  return 0;
}
