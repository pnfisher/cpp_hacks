#set args -O2 -S hack5.cpp
#set follow-fork-mode child
#catch exec

#set args -quiet -imultiarch x86_64-linux-gnu -D_GNU_SOURCE hack5.cpp -dumpbase hack5.cpp -mtune=generic -march=x86-64 -auxbase hack5 -O2 -o hack5.s

#b main
#b final_scan_insn

set environment LD_LIBRARY_PATH=/usr/local/lib:/home/phil/opt/trusty/x86_64/lib

