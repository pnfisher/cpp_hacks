#include <iostream>


class _AAA_ {

  static const int size = 10;

 public:

  int * v;

  _AAA_() {
    v = new int[size];
    for(int i = 0; i < size; i++)
      v[i] = i;
  }
  ~_AAA_() { if (v != nullptr) delete [] v; }

  _AAA_(const _AAA_& lhs) {
    std::cout << "xxx copy\n";
    v = new int[size];
    for(int i = 0; i < size; i++)
      v[i] = lhs.v[i];
  }

  // _AAA_(_AAA_&& lhs) : v(lhs.v) {
  //   std::cout << "xxx move\n";
  //   lhs.v = nullptr;
  // }

  void report(void) {
    for(int i = 0; i < size; i++) {
      std::cout << v[i] << " ";
    }
    std::cout << std::endl;
  }

  void munch(_AAA_ x) {
    for(int i = 0; i < size; i++)
      v[i] += x.v[i];
  }
  
};

__attribute__((noinline))
_AAA_ foo() {

  _AAA_ a;
  return a;

}

__attribute__((noinline))
int bump(int x) {

  return x + 1;

}

int main(void) {

  _AAA_ dummy;
  dummy.report();

  _AAA_ dummy2;

  dummy.munch(dummy2);
  dummy.report();

  //_AAA_ dummy2(dummy);
  //dummy2.report();

  //_AAA_ dummy3(std::move(dummy));
  //dummy3.report();

  //int x = bump(10);

  return 0;

}
