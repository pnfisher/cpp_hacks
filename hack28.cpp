#include <array>
#include <algorithm>
#include <deque>
#include <iostream>
#include <list>
#include <vector>
#include <array>

#if 0
template <template <class, class...> class C, class V, class... A>
void
print_elements(const C<V, A...>& c)
{
  auto it = c.begin(), e = c.end();
  std::cout << *it;
  for (it++; it != e; it++)
    std::cout << ' ' << *it;
  std::cout << std::endl;
}
#else
template<typename T>
void
print_elements(const T& c) {
  auto it = c.begin(), e = c.end();
  std::cout << *it;
  for (it++; it != e; it++)
    std::cout << ' ' << *it;
  std::cout << std::endl;
}
#endif

int
main(void)
{

  std::vector<int> ints{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

  {
    auto ints2(ints);
    print_elements(ints2);

    // clang-format off
    auto b = ints2.begin() + 3,
         e = ints2.end() - 1,
         p = ints2.begin() + 4;
    // clang-format on

    std::rotate(b, p, e);
    print_elements(ints2);
    std::iter_swap(b, --e);
    print_elements(ints2);
    std::reverse(ints2.begin(), ints2.end());
    print_elements(ints2);

    nth_element(ints2.begin(), ints2.begin() + 6, ints2.end());
    print_elements(ints2);

    std::sort(ints2.begin(), ints2.end());
    print_elements(ints2);
    
  }

  return 0;
}
