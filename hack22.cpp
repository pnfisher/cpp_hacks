#include <iostream>
#include <future>
#include <vector>

uint64_t fib(uint32_t n) {

  if (n == 0) return 0;
  if (n <= 2) return 1;
  return fib(n-2) + fib(n-1);

}

static int count = 0;

struct Widget {

  int x, y;

  Widget() : x(count++), y(count++) {}
  Widget(const Widget& rhs) : x(rhs.x), y(rhs.y) {
    std::cout << "copy ctor, x = " << x << ", y = " << y << std::endl;
  }
  void inc(int i) { x += i; y += i; }
  void report(void) {
    std::cout << "x = " << x << ", y = " << y << std::endl;
  }

};

struct Widget2 {

  mutable int x;
  int y;

};

int main(void) {

  auto func = [](int in) { return in + 3; };
  auto sum = func(2);
  std::cout << "sum = " << sum << std::endl;

  const Widget2 W2 = { 10, 11 };
  W2.x = 3;
  

  Widget w1, w2;

  auto wp = std::pair<Widget, Widget>(w1, w2);
  w1.inc(10); w1.report();
  w2.inc(10); w2.report();
  Widget w3, w4;
  std::tie(w3, w4) = wp;

  w3.report();
  w4.report();

  auto f = std::async([]{ return std::pair<int, int>{2,3};});
  f.wait();
  int x, y;
  std::tie(x,y) = f.get();

  std::cout << "xxx? " << x << " " << y << std::endl;

  const uint32_t n = 10;
  std::vector<std::future<uint64_t>> v;
  for(int i = 0; i < 8; i++) {
    v.push_back(std::async(fib,n));
  }
  for(auto& fib: v) {
    fib.wait();
    std::cout << "fib(" << n << ") = " << fib.get() << std::endl;
  }

  return 0;

}
