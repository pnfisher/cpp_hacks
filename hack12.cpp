#include <cstdio>
#include <cassert>
#include <array>
#include <memory>

#include "benchmark.hpp"

template<typename T, uint32_t size>
struct Values {
  std::array<T, size> values;
  Values() {
    uint32_t i = 0;
    for(auto v: values) {
      v = i++;
    }
  }
  T sum() {
    T t = 0;
    for(auto i1 = values.begin() + 1, i2 = values.end(); i1 != i2; i1++) {
      t = t + *i1;
    }
    return t;
  }
  T multiply() {
    T t = 1;
    for(auto i1 = values.begin(), i2 = values.end(); i1 != i2; i1++) {
      t = t * *i1;
      if (!t)
        t = 1;
    }
    return t;
  }
};

template<typename T, uint32_t size>
T multiply(std::array<T, size>& values) {
  T t = (T)1;
  for(auto i1 = values.begin(), i2 = values.end(); i1 != i2; i1++) {
    t = t * *i1;
    if (!t)
      t = 1;
  }
  return t;
}

int main(void) {

  #define params uint32_t, 0x8
  struct Values<params> v;
  benchmark bm;
  uint32_t t = 0;

  bm.clear();
  bm.start();
  t = v.multiply();
  bm.stop();
  printf("result = %d, nsecs = %ld, secs = %f\n", t, bm.nsecs(), bm.secs());

  bm.clear();
  bm.start();
  t = multiply<params>(v.values);
  bm.stop();
  printf("result = %d, nsecs = %ld, secs = %f\n", t, bm.nsecs(), bm.secs());

  auto ptr = std::make_shared<std::array<int, 10>>();
  (*ptr.get())[5] = 5;


  
  return 0;

}
